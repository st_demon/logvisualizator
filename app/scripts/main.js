(function() {
  $(document).ready(function(){
  var dataSource;
  var max;
  $.get("/api/aggregateError")
    .done(function(data) {
      $("#load").addClass("hidden");
      max = d3.max(data, function (d) { return d.total});
      var sorted = sortSeparate(data);
          
      drawPies(sorted);
      tabulate(sorted[0], ["_id", "total"], "The most common errors"); 
      tabulate(sorted[1], ["_id", "total"], "Other"); 
    });

    function sortSeparate (data, callback)  {
        var other = {
            _id: "other",
            total: 0
        },
        main = [];
        otherArray = [];

        for(var i = 0; i < data.length; i++) {
          if (max / data[i].total > 55) {
            otherArray.push(data[i]);
            other.total += data[i].total;
          } else {
            main.push(data[i]);
          }
        }
        main.push(other);

        return [main, otherArray];
    }

    function drawPies (data) {
      var w = 300,                        
      h = 300,                            
      r = 150,

      color = d3.scale.category20b();
      //create Pies
      createChart(data[0], "Chart of the most common errors");
      createLegend(data[0]);
      // createChart(data[1], "Other");

      function createChart (data, header) {
        d3.select(".log-chart").append("h4").text(header);
        var vis = d3.select(".log-chart")
          .append("svg:svg")              
          .data([data])                   
              .attr("width", w)           
              .attr("height", h)
          .append("svg:g")               
              .attr("transform", "translate(" + r + "," + r + ")");
        

        var arc = d3.svg.arc()    
            .outerRadius(r)
            .innerRadius(75)

        var pie = d3.layout.pie()          
            .value(function(d) { return d.total; }); 

        var tooltip = d3.select("body")
            .append("div")
            .attr("class", "tooltip");

        var arcs = vis.selectAll("g.slice")     
            .data(pie)                          
            .enter()                            
                .append("svg:g")                
                    .attr("class", "slice");   
            //tooltip
            arcs.append("svg:path")
                    .attr("fill", function(d, i) { return color(i);})
                    .attr("d", arc)
                    .on('mouseover', function(d, i) {
                      tooltip.style("visibility", "visible");
                      tooltip.html("<p>Message: " + d.data._id + "<br>Total:" + d.data.total + "</p>")
                    })  
                    .on("mousemove", function(){return tooltip.style("top", (event.pageY-10)+"px").style("left",(event.pageX+10)+"px");})
                    .on("mouseout", function(){return tooltip.style("visibility", "hidden");});                           
            //text
            arcs.append("svg:text")                                     
                .attr("font-family", "monospace") 
                .style("font-weight", "bolder") 
                .text(function(d, i) { 
                  var text = "";
                  if (max/ data[i].total < 6){
                      text = data[i].total
                  }
                  return text; 
                })
                .attr("text-anchor", "middle")
                .attr("transform", function(d) {                    
                d.innerRadius = 0;
                d.outerRadius = r;
                return "translate(" + arc.centroid(d) + ")";
            })
      }

      function createLegend (data) {
        var legendRectSize = 18;
        var legendSpacing = 4; 
        var w = 600,                        
            h = 400;
        
        var legend = d3.select('.log-legend')
          .append('svg:svg')
          .attr("width", w)           
          .attr("height", h)
        legend.selectAll('.legend')
          .data(data)
          .enter()
          .append('g')
          .attr('class', 'legend')
          .attr('transform', function(d, i) {
            var height = legendRectSize + legendSpacing;
            var offset =  -4 * color.domain().length / 2;
            var horz = 0;
            var vert = i * height - offset;
            return 'translate(' + horz + ',' + vert + ')';
          });

        legend.selectAll('.legend').append('rect')                                     
          .attr('width', legendRectSize)                          
          .attr('height', legendRectSize)                         
          .style('fill',  function(d, i) { return color(i)});
          
        legend.selectAll('.legend').append('text')
          .attr('x', legendRectSize + legendSpacing)              
          .attr('y', legendRectSize - legendSpacing)
          .html(function(d) { return d._id; })
          .attr("font-family", "monospace") 
          .style("font-size", "0.95em"); 


      }
      
    }

    function tabulate(data, columns, header) {
      
      var table = d3.select(".log-table").append("table"),
          tcaption = table.append("caption").text(header),
          thead = table.append("thead").attr("style", "text-align: left;font-family: monospace"),
          tbody = table.append("tbody"),
          sortData = data.sort(function(a,b) {return b.total-a.total;});
    
          
      thead.append("tr")
          .selectAll("th")
          .data(["Message", "Total"])
          .enter()
          .append("th")
              .text(function(column) { return column; })


      var rows = tbody.selectAll("tr")
          .data(sortData)     
          .enter()
          .append("tr");

      var cells = rows.selectAll("td")
          .data(function(row) {
              return columns.map(function(column) {
                  return {column: column, value: row[column]};
              });
          })
          .enter()
          .append("td")
          .attr("style", "font-family: monospace; font-size: 0.95em;")
              .html(function(d) { return d.value; });
    }   
  });
})();
