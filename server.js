var express = require("express"), 
	cons = require("consolidate"), 
	app = express(), 
	MongoClient = require('mongodb').MongoClient, 
    assert = require('assert'),
    dbUrl = 'mongodb://localhost:27017/analyze';

app.use(express.static(__dirname + '/app'));
 
app.get('/api/aggregateError', function (req, res) {

	MongoClient.connect(dbUrl, function(err, db) {
	  	assert.equal(null, err); 
	  	var collection = db.collection('logs'),
	  		responceDB = [],
	  		cursor = collection.aggregate([{$project : {level : 1, message : 1, count: {$add: [1]}}},
	    									{$match: {level: 'ERROR'}},
	    										{$group: { _id: "$message", total : { $sum: "$count"}}}]);
	    
	    cursor.on('data', function(doc) {
	      responceDB.push(doc);
	    });

	    cursor.once('end', function() {
	    	res.send(responceDB);
	    	db.close();
	    });

	});

});

app.listen(8000); 
console.log("App listen on 8000");