# README #

### Requirements ###

* Node.js
* MongoDB

### Install ###

```
#!prompt

npm install
``` 
### Getting Started ###
Export your logs to DB

```
#!prompt

mongoimport --db analyze --collection logs --type csv --headerline --file D:\myWorkspace\SANDBOX\LogVisualizator\data\some_logs.csv
```
Start DB

```
#!prompt

mongod
```

Start server
```
#!prompt

node server
```
See the result on http://localhost:8000/